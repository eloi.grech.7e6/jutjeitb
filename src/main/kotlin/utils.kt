

/**
 * @author Eloi Fàbrega
 * @version Versió 1, Data: 08/01/2023
 * Asks the user to input an integer within a given range and returns the input value.
 * @param min The minimum value that the user can input.
 * @param max The maximum value that the user can input.
 * @return The integer value that the user inputs.
 * @throws NumberFormatException if the user input is not a valid integer.
 */
fun askIntRange(min: Int, max: Int): Int {
    var usrSelection: Int = min
    var isValidInput = false
    while (!isValidInput) {
        val input = askIntSafe()
        if (input in min..max) {
            usrSelection = input
            isValidInput = true
        } else {
            println("${red("Introdueix un nombre entre $min i $max")}")
        }
    }
    return usrSelection
}

/**
 * Asks the user to input an integer and returns the input value if it is valid.
 * @return The integer value that the user inputs.
 * @throws NumberFormatException if the user input is not a valid integer.
 * @author Eloi Fàbrega
 * @version Versió 1, Data: 08/01/2023
 */
fun askIntSafe(): Int {
    var usrSelection: Int? = null
    var isValidInput = false
    while (!isValidInput) {
        try {
            usrSelection = readln().toInt()
            isValidInput = true
        } catch (e: NumberFormatException) {
            println("${red("Input invalid, intenta de nou")}")
        }
    }
    return usrSelection!!
}



/**
 * @author Eloi Fàbrega
 * @version Versió 1, Data: 08/01/2023
 * crlear the screen (or print 50 blank lines)
 */
fun cls(lines:Int = 50){
    if(DEBUGGING){
        println("\n"+"#".repeat(lines)+"\n")
    }else {
        println("\n".repeat(lines))
    }
}

/**
 * Calculates the length of a given string str without taking into account any color codes that may be present in the string.
 * @param str the string whose length is to be calculated
 * @return the length of the string without any color codes
 * @author Eloi Fàbrega
 * @version Versió 1.0, Data: 11/03/23
 */
fun lengthWithoutColorCodes(str: String): Int {
    val regex = "\u001b\\[\\d{1,2}m".toRegex() // regular expression to match color codes
    return regex.replace(str, "").length // remove color codes and calculate length
}

/**
 * Generates an ASCII table with headers and content and prints it to the console.
 *
 * @param headers A list of strings with the headers of the table
 * @param content A list of lists of strings with the content of the table
 * @author Eloi Fàbrega
 * @version Versió 1.0, Data: 02/03/2023
 */
fun printAsciiTable(headers: List<String>, content: List<List<String>>,dataSeparator:Boolean=false) {
    // ┌ ┬ ─ ┐
    // ├ ┼ ─ ┤
    // │ │   │
    // └ ┴ ─ ┘
    val paddingEnd = 2
    val paddingBegin = 1
    val vv = '│'
    val hh = '─'
    val cc = '┼'
    val tl = '┌'
    val tt = '┬'
    val tr = '┐'
    val ll = '├'
    val rr = '┤'
    val bl = '└'
    val bb = '┴'
    val br = '┘'



    // Find the maximum length for each column
    val columnWidths = MutableList(headers.size) { 0 }
    for (i in headers.indices) {
        // initialize to header Length
        columnWidths[i] = lengthWithoutColorCodes(headers[i]) + paddingEnd + paddingBegin
        for (j in content.indices) {
            val contentLength = lengthWithoutColorCodes(content[j][i]) + paddingEnd + paddingBegin

            if (contentLength > columnWidths[i]) {
                columnWidths[i] = contentLength
            }
        }
    }




    //Helper function that prints either a spacer or a line with data
    fun rowLine(valuesOrLine:Any, collision:Char, leftChar: Char, rightChar: Char){

        val result = mutableListOf<String>()

        for (i in columnWidths.indices) {
            var text:String = ""

            if(valuesOrLine is List<*>){
                text = " ".repeat(paddingBegin)
                text += valuesOrLine[i].toString()//.padEnd(columnWidths[i], ' ')
                text  = ensureLengthString(text,columnWidths[i])
            }else if(valuesOrLine is Char){
                //if is char, example "-" repeat the char for the whole line
                text = valuesOrLine.toString().repeat(columnWidths[i])
            }
            result.add(text)
        }
        println(leftChar + result.joinToString(collision.toString()) + rightChar)
    }


    //TOP LINE
    rowLine(hh,tt,tl,tr)

    // Print the table header
    rowLine(headers,vv,vv,vv)

    //print divider
    rowLine(hh,cc,ll,rr)

    // Print the table content
    for (i in content.indices) {// line
        rowLine(content[i],vv,vv,vv)
        if(dataSeparator && i < content.size-1){
            rowLine(hh,cc,ll,rr)
        }
    }

    //print bottom divider
    rowLine(hh,bb,bl,br)
    println("\n")
}


/**
 * Ensures the length of a given string str. If the length of the string without color codes is greater than the provided length parameter,
 * the string is trimmed and "..." is added to the end. If the length of the string without color codes is less than or equal to length,
 * the string is padded with spaces at the end.
 * Before any operation, this function replaces all occurrences of "\n" with "" in the input string.
 * @param str the string to format.
 * @param length the desired length of the string.
 * @return the formatted string with the desired length.
 * @author Eloi Fàbrega
 * @version 2.0, 011/03/2023
 */
fun ensureLengthString(str: String, length: Int): String {
    val cleanedStr = str.replace("\n", "")
    val trimmedLength = lengthWithoutColorCodes(cleanedStr)

    return if (trimmedLength > length) {
        cleanedStr.substring(0, length - 3).trimEnd() + "..."
    } else {
        val spacesToAdd = length - trimmedLength
        cleanedStr + " ".repeat(spacesToAdd)
    }
}

/**
 * Returns the provided txt string formatted in red color using ANSI escape codes.
 * @param txt the string to format in red color
 * @return the red-formatted string
 * @version 1.0, 11/03/2023
 * @author Eloi Fàbrega
 */
fun red(txt:String):String{
    return col["red"] + txt + col.get("reset")
}

/**
 * Returns the provided txt string formatted in green color using ANSI escape codes.
 * @param txt the string to format in red color
 * @return the green-formatted string
 * @version 1.0, 11/03/2023
 * @author Eloi Fàbrega
 */
fun green(txt:String):String{
    return col["green"] + txt + col.get("reset")
}

/**
 * Asks the user to input a value and returns it as a string.
 * @param txt the text prompt to display to the user before reading input
 * @return the string input entered by the user
 * @version 1.0, 11/03/2023
 * @author Eloi Fàbrega
 */
fun askAndRead(txt:String):String{
    println(txt)
    return readln()
}
