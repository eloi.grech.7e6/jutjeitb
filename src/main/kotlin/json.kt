import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

/**
 * Encodes the provided obj parameter to a JSON string and saves it to the specified file. Returns true if the operation was successful,
 * and false otherwise.
 * @param file the file to which the JSON data should be saved
 * @param obj the object to encode and save as JSON
 * @return true if the operation was successful, false otherwise
 * @author Eloi Fàbrega
 * @version 2.0, 011/03/2023
 */
inline fun <reified T> saveJson(file: File, obj: T): Boolean {
    return try {
        val jsonText = Json { prettyPrint = true }.encodeToString(obj)
        file.writeText(jsonText)
        true
    } catch (e: Exception) {
        false
    }
}

/**
 * Reads the contents of the specified file as a JSON-encoded string and decodes it into an object of type T.
 * @param file the file containing the JSON-encoded data
 * @return an object of type T decoded from the JSON-encoded data
 * @throws SerializationException if the JSON data cannot be decoded into an object of type T
 * @author Eloi Fàbrega
 * @version 1.0, 011/03/2023
 */
inline fun <reified T> readJson(file: File): T {
    val fileData = file.readText()
    return Json.decodeFromString<T>(fileData)
}

fun saveDataUsers(users:MutableList<User>):Boolean{

    val file = File("./data/users.json")
    val success =  saveJson(file,users);
    if(!success){
        println(red("No s'ha pogut guardar correctament")+
                "\n comproba que no estas editant el arxiu i toraho a intentar desde el menu.")
    }
    return success
}

fun saveDataExercises(users:MutableList<Exercise>):Boolean{

    val file = File("./data/exercises.json")
    val success =  saveJson(file,users);
    if(!success){
        println(red("No s'ha pogut guardar correctament")+
                "\n comproba que no estas editant el arxiu i toraho a intentar desde el menu.")
    }
    return success
}

fun saveAll(users: MutableList<User>,exercises:MutableList<Exercise>){
    var correct = saveDataUsers(users)
    correct = correct && saveDataExercises(exercises)
    if(correct){
       cls()
        println(green("Guardat correctament."))
    }
}