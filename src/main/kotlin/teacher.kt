/**
 * given a teacher, shows all the students related to the teacher,
 * and show some information about the progress of the students
 *
 * @param teacher the teacher who is watching the student
 * @param users the list of all users in the system
 * @param container the container of exercises
 * @version 1.0, 11/03/2023
 * @author Eloi Fàbrega
 */
fun llistarAlumnes(teacher: User, users:MutableList<User>, container: ExerciseContainer){
    cls()

    val myStudents = getTeacherStudents(teacher,users)
    val listOfData = mutableListOf<MutableList<String>>()

    for (st in myStudents){
        val count = st.countExercisesSolved()
        val total = container.list.size
        val exercisesDone = if (count > total / 2) green("$count/$total")
                            else red("$count/$total")

        val line = mutableListOf<String>(
            st.name.replaceFirstChar{ it->it.uppercase()},
            st.email,
            exercisesDone
        )
        listOfData.add(line)
        //println("${st.name.replaceFirstChar { it.uppercase() }} | ${st.email}")
    }

    printAsciiTable(listOf("Nom","Correu","Exercisis fets"),listOfData.toList(),false)

}

/**
 * Returns a list of students who are taught by the given teacher.
 * @param teacher the teacher whose students to retrieve
 * @param students the list of all students in the system
 * @return a mutable list of students taught by the given teacher
 * @author Eloi Fàbrega
 * @version Versió 1.0, Data: 1/03/23
 */
fun getTeacherStudents(teacher: User, students:MutableList<User>):MutableList<User>{
    val result = mutableListOf<User>()

    for(st in students){
        if(!st.isTeacher && st.teacher == teacher.email){
            result.add(st)
        }
    }

    return result
}

/**
 * Adds a new student to the given list of students,
 * The function asks the user for the student's name and email address,
 * if not exists, asks for confirmation before creating and adding the new student to the list.
 *
 * @param teacher the teacher who is adding the new student
 * @param users the list of all users in the system
 * @param clear whether to clear the console before prompting the user for input (default true)
 * @version 1.0, 11/03/2023
 * @author Eloi Fàbrega
 */
fun afegirAlumne(teacher: User, users: MutableList<User>, clear:Boolean = true){
    //TODO put an option to quit without adding any user
    if(clear){
        cls()
    }

    val name = askAndRead("Introdueix el nom del alumne:")
    val email = askAndRead("Introdueix la direccio de correu del alumne:")

    //check if the student already exists
    if(users.any { it.email == email }){
        cls()
        println(red("Ja existeix un usuari amb aquest correu"))

        //ask again recursively
        return afegirAlumne(teacher,users,false)
    }

    //ask confirmation
    println("Les dades introduides son:\n" +
            "\tNom: $name\n" +
            "\tEmail: $email")

    val usrOpt = printMenu(listOf("Si","No"),false,"Les dades son correctes?")

    //if data is wrong ask again recursiveley
    if(usrOpt==2) return afegirAlumne(teacher,users)

    //create the student
    val student = User(name,email,false, mutableListOf(),"",teacher.email)

    //save it
    users.add(student)
    saveDataUsers(users)
}

/**
 * shows the teacher a list of students to choose one of them, then Shows the student progress in a table,
 *
 * @param teacher the teacher who is watching the student
 * @param users the list of all users in the system
 * @param container the container of exercises
 * @version 1.0, 11/03/2023
 * @author Eloi Fàbrega
 */
fun mostrarFeinaAlumne(teacher: User, users: MutableList<User>, container: ExerciseContainer){
    cls()
    val myStudents = getTeacherStudents(teacher,users)
    val myStudentsStr = myStudents.map { it.name + " - " + it.email }
    val usrOpt = printMenu(myStudentsStr,false,"Escull un alumne:") -1
    val student = myStudents[usrOpt]

    println("Report de exercisis de: ${student.name}")
    container.showList(student)
}