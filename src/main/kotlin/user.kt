import kotlinx.serialization.Serializable
import java.lang.Exception


@Serializable
open class User(
    var name: String,
    var email: String,
    var isTeacher: Boolean,
    var exercisesSolved: MutableList<Int> = mutableListOf(),
    var password: String = "",
    var teacher: String = "",
    val exerciseAttempts: MutableMap<Int, Int> = mutableMapOf()

){



    var exerciseIndex = -1;

//    fun getName():String{
//        return this.name.replaceFirstChar { it.uppercase() }
//    }

    fun listProblems(container: ExerciseContainer,users: MutableList<User>){
        val exercise = container.askExercise(this)
        container.playExercise(exercise,this,users)

    }

    fun incrementExerciseAttempts(exerciseId: Int) {
        val currentValue = exerciseAttempts.getOrDefault(exerciseId, 0)
        exerciseAttempts[exerciseId] = currentValue + 1
    }

    fun solveExercise(exerciseId: Int){
        if (!this.isExerciseSolved(exerciseId)) {
            exercisesSolved.add(exerciseId)
        }
    }

    fun isExerciseSolved(exerciseId: Int):Boolean{
        return exercisesSolved.contains(exerciseId)
    }

    fun getExerciseAttempts(exerciseId: Int): Int {
        return exerciseAttempts.getOrDefault(exerciseId, 0)
    }

    fun help(){
        if(this.isTeacher){
            println("Estas en el menu de Profesor, des de aqui podras gestionar el progres dels teus alumnes,\n" +
                    "afegir exercicis, i afegir o eliminar alumnes.\n" +
                    "Si tens algun dubte mes et pots adreçar al administrador del sistema.")
        }else{
            cls()
            println("Estas en el menu de Alumne, aqui podras escollir quins problemes vols resoldre,\n" +
                    "Tot depen de a quins reptes et veiguis capaçitat per resoldre,\n" +
                    "Tambe pots consultar el historic dels problemes ja resolts." +
                    "Si tens alguna pregunta més et pots adreçar al teu profesor personalment o a la adreça ${this.teacher}")
        }
    }


    fun countExercisesSolved():Int{
        return exercisesSolved.size
    }

    fun nextUnsolvedExercise(size:Int): Int {


        try {
            val nextIndex = this.getNextUnsolvedExerciseFrom(this.exerciseIndex,size)
            this.exerciseIndex=nextIndex
            return nextIndex
        }catch (e:Exception){
            return -1
        }
    }

    fun getNextUnsolvedExerciseFrom(from: Int,size: Int): Int {
        var next = (from + 1) % size
        while (this.exercisesSolved.contains(next)) {
            next = (next + 1) % size
            if (next == from) {
                // All exercises have been solved
                throw  Exception("All exercises have been solved")
            }
        }
        return next
    }

}




fun findUniqueUserByEmail(users: List<User>, prefix: String): User {
    val perfectMatchingUsers = users.filter { it.email == prefix }
    if(perfectMatchingUsers.size == 1){
        return perfectMatchingUsers[0]
    }

    val matchingUsers = users.filter{ it.email.startsWith("$prefix@") }

    if(matchingUsers.size==0){
        throw Exception("No coincidences")
    }else if (matchingUsers.size > 1) {
        throw Exception("Multiple coincidences")
    }

    return matchingUsers[0]
}





fun signIn(users:MutableList<User>):User{
    val email = askAndRead("\nintrodueix el correu:")
    var alert = ""



    //user asks for help
    if (email.lowercase() == "ajuda") {
        cls()
        println( "Introdueix el correu que t'ha proporcionat el centre educatiu, <nom.cognom.id@itb.cat>\n" +
                "Tambe pots accedir sense necessitat de introduir @itb.cat, nomes amb <nom.cognom.id>")

    }else{

        try {
            // may throw exception if no user found or multiple found
            val user = findUniqueUserByEmail(users, email)

            //if user has pasword, in the future maybe studens have paswords
            if (user.password != "") {
                //if correct return the user
                val correct = askPasword(user)
                if (correct) {
                    return user
                }else{
                    //try again
                    alert ="Contrasenya Incorrecta, Intents Esgotats"
                }
            }else{
                return user
            }

        }catch (e:Exception){
            alert = when(e.message){
                "No coincidences"-> "No s'ha trobat cap usuari"
                "Multiple coincidences" -> "Has de especificar la direccio de correu completa"
                else -> e.message.toString()
            }

        }

        cls()
        if(alert!=""){
            println( red(alert))
            println()
        }
        println("Escriu \"ajuda\" per mostrar ajuda")


    }


    //recursively if not found ask again
    return signIn(users)
}

fun askPasword(user: User, maxAttempts:Int = 3):Boolean{
    cls()

    for(t in 0 until maxAttempts){
        if(t>0){
            cls()
            val intents = maxAttempts-t
            if(intents == 1){
                println(red( "Contrasenya incorrecta, et queda un ultim intent"))
            }else{
                println(red( "Contrasenya incorrecta, et queden ${intents} intents"))
            }
        }

        println(user.email)
        val pswd = askAndRead("Introdueix la teva contrasenya:")
        if(pswd == user.password){
            return true
        }

    }
//        println(red( "Intents esgotats"))

    return false
}

//class Student(name: String) : User(name, false) {
//    val exercisesDone: MutableList<Int> = mutableListOf()
//}
//
//class Teacher(name: String, val password: String) : User(name, true)

