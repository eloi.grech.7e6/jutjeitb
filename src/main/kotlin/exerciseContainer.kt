
class ExerciseContainer(maxPrivateTest:Int){
    val list = mutableListOf<Exercise>()
    var exerciseIndex = 0
    var exercisesSolved = 0
    val maxPrivateTest = maxPrivateTest


    fun printCategories(){
        val map = mutableMapOf<String, Int>()

        for(ex in this.list){
            if(!map.containsKey(ex.type)){
                map[ex.type] = 0
            }
            map[ex.type] = (map[ex.type] ?: 0) + 1
        }

        for ((key, value) in map) {
            println("$key: $value")
        }


    }

    fun getExerciseById(id:Int):Exercise?{
        return this.list.find { it.id == id }
    }

    fun addExercise(){
        //TODO the exercise of one teacher will be added to all the students, is this OK?
        //TODO put an option to quit without adding any exercise

        fun askJocDeProves(testSet:MutableList<UserTest>){
            do{
                val input = askAndRead("Introdueix el ${green("Input")} (entrada quer rebra el programa)")

                val output = askAndRead("Introdueix el ${green("Output")} (Sortida que haura de tenir el programa)")

                testSet.add(UserTest(input,output))
                val usrOpt = printMenu(listOf("Si","No"),false,"Vols afegir un altre joc de proves?")
            }while (usrOpt!=2)

        }

        cls()

        val topics = listOf("list","algorithm","conditional","loop","dataType")
        val topicsShow = listOf("Llistes","Algorismica","Condicionals","Bucles","Tipus de dades")

        val public_test = mutableListOf<UserTest>()
        val private_test = mutableListOf<UserTest>()

        val statement = askAndRead("Introdueix el enunciad del exercisi:")


        var usrOpt = printMenu(topicsShow,false,"Escull la categoria")
        val topic = topics[usrOpt-1]

        println("Introduccio de jocs de proves visibles per el alumne, Formaran part del enunciat del problema")
        askJocDeProves(public_test)

        println("Introduccio de jocs de proves privats per el alumne, el alumne haura de encertar el output, donat un input\n" +
                "Cuants mès en posis millor, l'alumne nomes veura un parell d'ells pero si en poses mes, s'escollira un parell aleatori")
        askJocDeProves(private_test)


//TODO ask user confirmation if data is correct
//
//        //ask confirmation
//        println("Les dades introduides son:\n" +
//                "\tNom: $name\n" +
//                "\tEmail: $email")

//        val usrOpt = printMenu(listOf("Si","No"),false,"Les dades son correctes?")


//        //if data is wrong ask again recursiveley
//        if(usrOpt==2) return afegirAlumne(teacher,students)

        //create the exercise
        val id = getNextId()
        val exercise = Exercise(id,statement,topic,public_test,private_test)

        //save it
        this.list.add(exercise)
        saveDataExercises(this.list)

        cls()
    }

    fun getNextId(): Int {
        val usedIds = this.list.map { it.id }.toSet()
        var nextId = 0
        while (usedIds.contains(nextId)) {
            nextId++
        }
        return nextId
    }

    fun showList(user: User){
        val headers = listOf("ID","Enunciat","Categoria","Resolt","Intents")
        val listToPrint = mutableListOf<List<String>>()

        for (ex in this.list){
            val exercise = ex.getListString(user)
            listToPrint.add(exercise)
        }

        cls()
        printAsciiTable(headers,listToPrint,true)

    }

    fun add(ex:Exercise){
        this.list.add(ex)
    }

    fun add(list :MutableList<Exercise>){
        for(ex in list){
            this.list.add(ex)
        }
    }

    fun askExercise(user:User,cls:Boolean=true):Exercise{
        //TODO what if user wants to quit
        if(cls){
            cls()
            this.showList(user)
        }

        println("Introdueix la ID de un exercisi:")
        val id = askIntSafe()

        val exerciseFound = this.list.find { it.id == id }
        if (exerciseFound == null) {
            cls()
            this.showList(user)
            println(red("Introdueix la ID de un exercisi correctament siusplau:"))
            //if not found, ask again recursively
            return this.askExercise(user,false)
        }
        return exerciseFound
    }

    fun play(user: User,users: MutableList<User>){
        var exit = false

        var index = user.nextUnsolvedExercise(this.list.size)
        while(!exit){

            if(index == -1){
                println("Ja as resolt tots els problemes")
                //TODO if all exercises solved, ask the user if wants to start again and clear the list of exercisesDone
                exit = true
                return
            }

            val ex = list[index]

            println("Exercise ${index+1}:")
            ex.printStatement()

            var usrChoice = printMenu(listOf("Resoldre","Next"),true)



            when(usrChoice){
                0 -> exit=true
                1 -> {
                    val solved = this.playExercise(ex,user,users)
                    if(solved){
                        //si el resol salta al seguent
                        index = user.nextUnsolvedExercise(this.list.size)
                    }
                }
                2 -> {
                    index = user.nextUnsolvedExercise(this.list.size)
                    println(index)
                }
            }


        }//while

    }//play

    fun playExercise(ex: Exercise,user: User,users:MutableList<User>):Boolean{
        val solved = ex.play(maxPrivateTest,user)
        saveDataUsers(users)
        return solved
    }


}//ExerciseContainer
