import java.io.File

const val DEBUGGING = true


val col= mapOf(     //TEXT COLOR
    "red"     to "\u001b[31m",
    "reset"   to "\u001b[0m",
    "black"   to "\u001b[30m",
    "green"   to "\u001b[32m",
    "yellow"  to "\u001b[33m",
    "blue"    to "\u001b[34m",
    "magenta" to "\u001b[35m",
    "cyan"    to "\u001b[36m",
    "white"   to "\u001b[97m",
)


fun main(){

    //TODO
    // - make Student and Teacher Inherit from User
    // - make sure exercses imported from file are well formated, and no overlap two IDs
    // - reudce code by creating a fucntion that is chooseStudent(teacher,users)

    val usersFile = File("./data/users.json")
    val users = readJson<MutableList<User>>(usersFile)


    val exercisesFile = File("./data/exercises.json")
    val container = ExerciseContainer(2)


    val exercises = readJson<MutableList<Exercise>>(exercisesFile)
    container.add(exercises)
    if(DEBUGGING){
        container.printCategories()
    }

    cls()
    var user = signIn(users)


    cls()
    println("welcome ${user.name.replaceFirstChar { it.uppercase() }}")


    mainMenu(user,container,users)


}


/**
 * Aquesta funcio controla tot el flux del programa
 *
 * @param userProvided es el usuari que ha iniciat sesio
 * @param container es el contenidor d'exercisis carregats desde el fitxer
 * @param users es la llista complerta de usuaris carregats desde el fitxer
 */
fun mainMenu(userProvided: User,container:ExerciseContainer,users:MutableList<User>){
    var user = userProvided

    var exit = false

    do {

        if(user.isTeacher){
            teacherMenu(user,container,users)
        }else{
            studentMenu(user,container,users)
        }

        cls()
        val userInput = printMenu(listOf("Canviar d'usuari","No vull sortir"),true,"Estas segur que vols sortir?")

        cls()
        when(userInput){
            0 -> exit = true
            1 -> user = signIn(users)
        }

    }while(!exit)
}







