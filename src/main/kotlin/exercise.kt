import kotlinx.serialization.Serializable



@Serializable
data class Exercise(
    val id:Int,
    val statement: String,
    val type:String,
    val public_tests:List<UserTest>,
    val private_tests:List<UserTest>,
){
    fun printStatement(){
        println(statement)
        for( i in 0 until public_tests.size ){
            println(public_tests[i].input + " -> " + public_tests[i].output )
        }
    }

    fun play(maxPrivateTest:Int,student: User):Boolean{
        //TODO if exercise is already solved must add attempts??
        if(!student.isExerciseSolved(this.id)){
            student.incrementExerciseAttempts(this.id)
        }

        cls()
        this.printStatement()


        println("Resol el exercisi segons els seguents inputs:")



        val randSet = this.getRandom(this.private_tests,maxPrivateTest)

        var end = false
        var i = 0

        //ask each input
        while(!end && i < randSet.size) {
            println(randSet[i].input)
            val usrInp = readln()

            if (usrInp != randSet[i].output) { //user.fail
                end = true
                cls()
                println(red("ERROR: el resultat no es correcte"))
                return false
            }

            i++
        }

        student.solveExercise(this.id)
        cls()
        println(green("Felicitats, has solucionat el problema"))
        return true

    }

    fun getRandom(list:List<UserTest>,maxPrivateTest: Int):List<UserTest>{
        val shuffled = list.shuffled() // shuffle the input list

        // if the input list is shorter than or equal to maxPrivateTest, return it as is
        if (shuffled.size <= maxPrivateTest) {
            return shuffled
        }

        // otherwise, return the first maxPrivateTest elements of the shuffled list
        return shuffled.subList(0, maxPrivateTest)
    }

    fun getListString(user: User):List<String>{
        val result = listOf(
            this.id.toString(),
            ensureLengthString(this.statement,80),
            this.type,
            if(user.exercisesSolved.contains(this.id)) "${green("Resolt")}" else "${red("Incomplert")}",
            user.getExerciseAttempts(this.id).toString()
        )


        return result
    }
}
@Serializable
data class UserTest(
    val input:String,
    val output:String
)


