
fun studentMenu(user:User,container:ExerciseContainer,users:MutableList<User>){
    val llistaMenu = listOf(
        "Seguir amb l’itinerari d’aprenentatge",
        "Llista problemes",
        "Consultar històric de problemes resolts",
        "Ajuda"
    )



    var exit = false
    while(!exit){

        var userInput = printMenu(llistaMenu,true)

        when(userInput){
            0-> exit = true
            1-> container.play(user,users)
            2-> user.listProblems(container,users)
            3-> container.showList(user)
            4-> user.help()
            else -> println(red("NOT IMPLEMENTED YET"))
        }
    }

}
fun teacherMenu(teacher: User, container: ExerciseContainer, students:MutableList<User>){
    val llistaMenu = listOf(
        "Afegir un nou exercisi",
        "Mostrar feina de alumne",
        "Llistar alumnes",
        "Afegir alumne",
        "Guardar dades manualment"
        //"Eliminar alumne"
    )



    var exit = false
    while(!exit){
        var userInput = printMenu(llistaMenu,true)

        when(userInput){
            0 -> exit = true
            1 -> container.addExercise()
            2 -> mostrarFeinaAlumne(teacher,students,container)
            3 -> llistarAlumnes(teacher,students,container)
            4 -> afegirAlumne(teacher,students)
            5 -> saveAll(students,container.list)
            else -> println(red("NOT IMPLEMENTED YET" ))
        }

    }


}

/**
 * Prints a menu with the given list of options and prompts the user to select an option.
 * By default, the menu includes an "exit" option at index 0, but this can be disabled by setting showExitOption to false.
 * The menu title can also be customized with the title parameter, and the text for the exit option can be customized with exitText.
 * @param list the list of menu options to display
 * @param showExitOption whether to include an "exit" option in the menu (default true)
 * @param title the title to display above the menu (default "MENU")
 * @param exitText the text to display for the exit option (default "SORTIR")
 * @return the index of the selected option (1-based), or 0 if the user selects the exit option
 * @author Eloi Fàbrega
 * @version Versió 1.0, Data: 1/03/23
 */
fun printMenu(list:List<String>, showExitOption:Boolean=true, title:String = "MENU",exitText:String="SORTIR"):Int{

    if(title!=""){
        println(title)
    }

    var index = 1
    for(element in list){
        println("\t${index} - ${element}")
        index++
    }

    if(showExitOption){
        println("\t0 - $exitText")
        return askIntRange(0,list.size)
    }

    return askIntRange(1,list.size)
}

